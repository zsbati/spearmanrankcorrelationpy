dicx = dict()
dicy = dict()
n = int(input().strip())
xstr = input().strip()
ystr = input().strip()
xlist = xstr.split(" ")
ylist = ystr.split(" ")
x = []
y = []
for i in range(len(xlist)):
  x.append(float(xlist[i]))
  y.append(float(ylist[i]))
  
  
for i in range(len(x)):
  dicx[i] = x[i]
  dicy[i] = y[i]
sortedX = sorted(dicx.values())
sortedY = sorted(dicy.values())
element = 0.0
index = 0
xrank = []
yrank = []

for i in range(len(x)):
  element =  x[i]
  for j in range(len(x)):
    if sortedX[j] == element:
      xrank.append(len(x)-j)  

for i in range(len(y)):
  element =  y[i]
  for j in range(len(y)):
    if sortedY[j] == element:
      yrank.append(len(y)-j)

sum = 0.0
for i in range(len(x)):
  sum += (xrank[i]-yrank[i])*(xrank[i]-yrank[i])

coeff = 1-6.0*sum/(n*(n*n-1))
print(coeff)
